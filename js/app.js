//(function() {
    var app = angular.module('ibjacApp', ['ngSanitize', 'ngRoute', 'ngAnimate']);
    
    app.config(function ($routeProvider) { 
      $routeProvider 
        .when('/menu-overfly', { 
          controller: 'ibjacController',
          templateUrl: 'js/templates/menu.html' 
        }) 
        .when('/proj/:indx', { 
          controller: 'workController',
          controllerAs: 'workCtrl',
          templateUrl: 'js/templates/sample-work.html' 
        }) 
        .when('/', { 
          controller: 'ibjacController'
          //templateUrl: 'js/templates/content-body.html' 
        }) 
        .otherwise({ 
          redirectTo: '/' 
        }); 
    });

    app.controller('ibjacController', ['$scope', '$sce', '$location', '$routeParams', function($scope, $sce, $location, $routeParams) {
    
        // Read URL param "on". URL format: http://ibjac.com/angular/#/?on=1
        $scope.dispdetail = $location.search().on; // Alternative to reading QS param
        //$scope.dispdetail = $routeParams.on;
        //$scope.dispdetail = '$routeParams.on';
        
        // Used to display content overlay
        $scope.dispwork = false;
        $scope.setdisp = function(val) {
            $scope.dispwork = val;
        }

        $scope.skillslist = [
            {title: 'Web Engineer', caption: 'Architect and build web sites and applications.'},
            {title: 'Efficiency Analyst', caption: 'Analyze, identify, improve, and repeat.'},
            {title: 'Team Lead', caption: 'Apply individual capabilities to specific needs.'},
            {title: 'Site Optimization', caption: 'Load time, cross-browser, SEO, and more.'},
            {title: 'Technical Writer', caption: 'Gather requirements, be concise, provide use cases.'},
            {title: 'Vendor/Talent Vetting', caption: 'Know your audience, ask the right questions.'}
            ];

        $scope.entrepreneurship = [
            //{date: '2013 - Present', title: 'Co-Founder & CEO', company: '<a href="http://audionit.com" target="_blank">AudiOnIt, Inc.</a>', descrip: 'AudiOnIt is a startup technology company that has developed a process to take voice and video messages recorded via phone or web and transferring them on to elegantly designed and manufactured audio/video cards.'},
            {date: '2011 - 2012', title: 'Founder & CTO', company: 'CountM, Inc.', descrip: 'CountM was developing an egress and ingress tracking platform for various industries.'},
            {date: '2000 - 2005', title: 'Founder & President', company: 'WorldNightClubs / NightLifeConnect, LLC.', descrip: 'A social network and online marketing company for the nightclub and entertainment industries covering all of Southern California. Unique in the industry and a pioneer of its time.'}
            ];

        $scope.employment = [
            // Using $sce as a sample for an individual piece of content
            {date: '2013 - Present', title: 'Web Producer', company: 'Teleflora', link: 'works/teleflora.html', descrip: $sce.trustAsHtml('<p><strong>Responsibilities include:</strong> frontend web development (HTML, CSS, jQuery, JavaScript), enterprise content management system, documentation, technical spec writing, vendor vetting, training, task management, process automation</p>'), achievetitle: 'Achievements', achievements: [// $sce not being used because ngSanitize is being used globally
                {title: '<u>Automated Email Generation</u><br />', descrip: 'Created an semi-automated process to generate responsive HTML emails cutting email coding from an average of 60 minutes to 5 minutes.<br />(<em>Technologies: XML, XSL, Windows Shell</em>)'},
                {title: '<u>Platform Lead</u><br />', descrip: 'As the lead of user documentation for the custom implementation of a new e-commerce platform, I led all major content updates and custom frontend features and functions.<br />(<em>Technologies: BCC, Endeca, JavaScript, jQuery, HTML, CSS</em>)'},
                {title: '<u>Vendor Expenditure Reduction</u><br />', descrip: 'Identified existing requirements and after a comprehensive vetting process replaced a legacy vendor with new one.<br />(<em>Results: $90,0000 yearly savings</em>)'}
                ]
            },
            //{date: '2013 - Present', title: 'Subject Matter Expert', company: 'Aquent', link: 'http://aquent.com', descrip: 'Part of an Expert Network of senior level digital talent with experience in front-end Web Development with the mandate of conducting technical interviews and providing feedback to recruiters about a candidate\'s coding skills to facilitate job placement.', achievetitle: 'Achievements', achievements: [{title: '<u>Voted "Most Useful Reviews" in 2015</u><br />', descrip: 'Over 500 interviews to date with an average with an average of 20 per month.'}]},
            {date: '2008 - 2013', title: 'Consultant', company: 'Self-Employed', descrip: 'Clients/Projects included: <a href="http://verve8media.com" target="_blank">Verve 8 Media</a>, <a href="http://www.cr8rec.com/" target="_blank">Creative Recreation</a>, <a href="http://olympusamerica.com" target="_blank">Olympus Cameras</a>, <a href="http://www.gsn.com/" target="_blank">Game Show Network</a>, Olympus Cameras, Kyocera, <a href="https://www.tsovet.com/" target="_blank">TSOVET</a>, <a href="http://nartools.com" target="_blank">National Association of Realtors</a>, Drum-Squad, <a href="http://www.opengatecapital.com/" target="_blank">Open Gate Capital</a>, <a href="http://www.omgglobal.com/" target="_blank">OMG Global</a>, NOSOTROS, <a href="http://pvartcenter.org/" target="_blank">P.V. Art Center</a>, <a href="http://drmarion.com/" target="_blank">Dr. Marion</a>, Insomnia Media, <a href="http://www.thompsonchemists.com/" target="_blank">Thompson Chemists</a>, <a href="http://www.help4srs.org/" target="_blank">H.E.L.P.</a>, <a href="http://www.sigulerguff.com/" target="_blank">Siguler Guff</a>', achievetitle: 'Technologies', achievements: [
                {descrip: 'HTML5, CSS3, JavaScript, jQuery, PHP, MySQL, Magento, WordPress, 3dCart, Drupal'}
                ]
            },
            // NEED TO FIX LINK
            {date: '2011', title: 'UX Prototyper', company: 'Belkin', link: 'http://belkin.com', descrip: 'Protoyped a web-based dashboard for pre-launch IoT electrical meter.', achievetitle: 'Technologies', achievements: [{descrip: 'JavaScript, jQuery'}]
            },
            {date: '2009', title: 'Instructor', company: $sce.trustAsHtml('<a href="http://www.cerritos.edu/" target="_blank">Cerritos College</a> / <a href="http://pvartcenter.org" target="_blank">P.V. Art Center</a>'), descrip: 'Taught beginning web development & introduction to social media.'},
            {date: '2007', title: 'Project Lead', company: '<a href="http://www.digitalfirstmedia.com/" target="_blank">Media News Group</a> / <a href="http://la.com" target="_blank">LA.com</a>', descrip: 'Consolidated the entertainment section of 11 local newspapers on to one large entertainment hub.', achievetitle: 'Technologies',
                achievements: [{descrip: 'Velocity, cmPublish'}], testimonials: [
                    {person: 'Tom Zevallos', avatar: 'tomzevallos.jpg', profile: 'https://www.linkedin.com/in/tom-zevallos-7931625', caption: 'Worked with Jerry - January 21, 2010', quote: 'I\'ve worked with Jerry at both the Torrance Daily Breeze and LA.com. Jerry is a well rounded developer with great communication skills. He has both the ability to effectively manage a technical team and bring understanding and support for a executive team.</p><p>On several projects we worked on, he was able to build sites and businesses from the ground up through his unique problem solving and dedication. Jerry is the type of individual who can help clear out any fog from the technical side, and can deliver answers and results by presenting the options.', company: 'Conversant, Inc.', position: 'Business Development Manager', companyurl: 'http://www.conversantmedia.com'}
                ]
            },
            {date: '2004 - 2007', title: 'Webmaster', company: 'The Daily Breeze', link: 'http://dailybreeze.com', descrip: 'Oversaw all web operations for the newspaper under I.T. Director.', achievetitle: 'Technologies',
                achievements: [{descrip: 'HTML, CSS, JavaScript, ASP, SQL'}], testimonials: [
                    {person: 'Julia Parton', avatar: 'juliaparton.jpg', profile: 'https://www.linkedin.com/in/juliaparton', caption: 'Managed Jerry indirectly - February 3, 2010', quote: 'I am pleased to give strong recommendation to anyone reviewing Jerry Cornejo\'s work. While working with Jerry at Copley Los Angeles Newspapers, Jerry handled many challenging assignments that let to increase revenue generation and increased online readership. Jerry is skilled at understanding the needs of all levels of management, as well as all employees involved in the project. He works extremely well in a team environment. Just recently I had connected again with Jerry for some assistance for a non-profit organization (dealing with aging issues) website. The site was old, unsearchable and needed some user flexibility. Jerry was able to assist us, pro-bono, with creating new templates to allow us to have a more user-friendly and updated website. Jerry is very dedicated and loyal.', company: 'Premier Bank of Palos Verdes', position: 'Vice President Corporate Development', companyurl: 'http://www.ibankpremier.com'},
                    {person: 'Phil Lawrence', avatar: 'phillawrence.jpg', profile: 'https://www.linkedin.com/in/plawrence5', caption: 'Reported directly to Jerry - January 21, 2010', quote: 'Jerry has a great understanding of how to best use technology to meet business objectives and his technical skills are excellent. He is also very strong as a manager and during the time I reported to him I felt like I was used effectively and was able to grow my technical skills so that I was more valuable to the company.', company: 'Digital First Media', position: 'Web/Mobile Strategist and Developer', companyurl: 'http://dailybreeze.com'},
                    {person: 'Jeannie Bolio', avatar: 'jeanniebolio.jpg', profile: 'https://www.linkedin.com/in/jeanie-bolio-6646784', caption: 'Worked directly with Jerry - January 21, 2010', quote: 'Working with Jerry was a wonderful experience. Jerry is both professional and Knowledgeable in all aspects of the Internet Operations. I was well educated from many of the meetings and training sessions with JJerry. I highly recommend Jerry Cornejo and am happy to endorse him and his work ethic and experience.', company: 'Moontide Media', position: 'Advertising Account Executive', companyurl: 'http://moontidemedia.com'}
                ]
            },
            {date: '1997 - 2004', title: 'Senior Web Technologist', company: 'Envision Group', link: 'http://envisiongroup.com', descrip: 'Started career as a full stack developer working on online presence and wed initiatives for Fortune 1000 companies.', achievetitle: 'Technologies',
                achievements: [{descrip: 'HTML, JavaScript, TCL, Perl, Struts, Illustra, ASP, Com Objects, SQL'}], testimonials: [
                    {person: 'Carey Reisz', avatar: 'careyreisz.jpg', profile: 'https://www.linkedin.com/in/careyreisz', caption: 'Worked directly with Jerry - November 24, 2011', quote: 'As a designer - I absolutely love working with Jerry. He takes a designer\'s visions - and easily re-constructs them so that the design team and programming team are BOTH happy with the results. Jerry is a fantastic programmer, and above all he knows how to communicate tech needs to a design team - in terms a designer can understand and deliver to.', company: 'Thrive Market', position: 'Senior Graphic Designer', companyurl: 'http://thrivemarket.com'},
                    {person: 'Sandy Johnson', avatar: 'sandyfu.jpg', profile: 'https://www.linkedin.com/in/sandyfujohnson', caption: 'Managed Jerry indirectly - January 26, 2010', quote: 'I worked with Jerry for many years at Envision Group. I was a Sr. Acct Executive/Sr. Producer and I had to work with many groups to achieve what our clients asked of us. Sometimes our asks were not so reasonable, but you would never get a complaint from Jerry (whereas most others would have lots to say). He is one the most hard-working, reliable guys I know. As we were creating strategies that didn\'t quite fit to a capability we had (YET), Jerry would also pull the latest books to study on how to get it done...the right way. I really miss working Jerry! Jerry is very easy-going, has lots of experience and can adapt to any environment. I think you would be very lucky to hire Jerry as a part of your team. Please feel free to email or call me if you need further details.', company: 'Fujo, Inc.', position: 'President/Co-Founder', companyurl: 'http://www.fujoinc.com'}
                ]
            },
        ];

        $scope.skillterms = ['HTML5', 'CSS3', 'jQuery', 'JavaScript', 'Bootstrap', 'SASS', 'RWD', 'XML', 'XSL', 'PhoneGap','PHP',
                             'MySQL', 'SEO', 'ATG', 'Endeca', 'Magento', 'WordPress', 'Drupal', 'Shell', 'AJAX', 'API', 'EDM', 'ESP', 'Apache', 'Angular', 'Optimization', 'LINUX', 'SQL', 'Facebook', 'Android'];

        $scope.projects = [
            {title: 'Teleflora', caption: 'Frontend, ATG', link: '#/proj/0', cats: 'fed cms',
                slides: [{img: 'teleflora1.jpg', alt: 'Home Page'}, {img: 'teleflora2.jpg', alt: 'Collection'}]
                },
            {title: 'Sample Spec Doc', caption: 'Technical Writing', link: '#/proj/1', cats: 'other', img: 'spec.png'},
            {title: 'Email Generator', caption: 'Frontend, Backend, Other', link: '#/proj/2', cats: 'fed other',
                slides: [{img: 'email1.jpg', alt: 'Responsive Emails'}, {img: 'email2.jpg', alt: 'Design View'}]
                },
            {title: 'LA.com', caption: 'Frontend, cmPublish', link: 'http://la.com', cats: 'fed cms', img: 'lacom.jpg'},
            /*{title: 'I Be J.A.C.', caption: 'HTML5, CSS3, jQuery, PHP, MySQL, more...', link: 'works/ibjac.html', cats: 'fed bed other',
                slides: [{img: 'ibjac1.jpg'}, {img: 'ibjac2.jpg'}]
                },*/
            {title: 'Creative Recreation', caption: 'Frontend, Backend, Magento', link: 'http://www.cr8rec.com', cats: 'fed cms bed', img: 'cre8rec.jpg'},
            {title: 'OMG Global', caption: 'Frontend, Drupal', link: 'http://omgglobal.com', cats: 'fed cms bed', img: 'omgglobal.jpg'},
            {title: 'TSOVET', caption: '3D Cart', link: 'http://tsovet.com', cats: 'cms', img: 'tsovet.jpg'},
            {title: 'National Association of Realtors', caption: 'Backend', link: 'http://nartools.com', cats: 'bed', img: 'nar.jpg'},
            {title: 'Palos Verdes Art Center', caption: 'Business Catalyst', link: 'http://pvartcenter.org', cats: 'cms', img: 'pvac.jpg'},
            {title: 'NightLifeConnect', caption: 'Frontend, Backend, PHPfox', cats: 'fed bed cms', img: 'nlc.jpg'},
            {title: 'Thompson Chemists', caption: 'Magento', link: 'http://www.thompsonchemists.com', cats: 'cms', img: 'thompson.jpg'},
            {title: 'Siguler Guff', caption: 'Backend', link: 'http://www.sigulerguff.com', cats: 'bed', img: 'sigulerguff.jpg'},
            {title: 'Game Show Network', caption: 'Backend', cats: 'bed', img: 'gsn.jpg'},
            {title: 'H.E.L.P.', caption: 'WordPress', link: 'http://www.help4srs.org', cats: 'cms', img: 'help.jpg'},
            {title: 'Dr. Marion', caption: 'Drupal', link: 'http://drmarion.com', cats: 'cms', img: 'drmarion.jpg'},
            ];

        $scope.companies = [
            {name: 'Teleflora', link: 'http://www.teleflora.com', img: 'teleflora.png'},
            {name: 'Belkin', link: 'http://belkin.com', img: 'belkin.png'},
            {name: 'Neutrogena', link: 'http://neutrogena.com', img: 'neutrogena.png'},
            {name: 'Samsung', link: 'http://samsung.com', img: 'samsung.png'},
            {name: 'Mazda', link: 'http://mazdausa.com', img: 'mazda.png'},
            {name: 'Isuzu', link: 'http://www.isuzu.com/home.jsp', img: 'isuzu.png'},
            {name: 'Olympus', link: 'http://olympusamerica.com', img: 'olympus.png'}
            ];

    }]);

//})();
//(function() {
//---------------------------//
// Remove spaces from string //
//---------------------------//

    app.filter('remSpaces', [function() {
        return function(string) {
            return string.replace(/[\s]/g, '');
        };
    }]);

//})();
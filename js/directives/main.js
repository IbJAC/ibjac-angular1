//(function() {
//------------------------------//
// Display individual employers //
//------------------------------//

    app.directive('prevEmployment', function() {
        return {
            restrict: 'A',
            templateUrl: 'js/templates/prev-employment.html',
            link: function(scope) {
                if (scope.job.link != null) {
                    if (!scope.job.link.match(/http/i)) {
                        scope.linkclss = 'link-work';
                    }
                    scope.job.company = '<a href="'+ scope.job.link +'" class="'+ scope.linkclss +'" target="_blank">'+ scope.job.company +'</a>';
                }
            }
        }; 
    });


//-----------------------//
// Display company logos //
//-----------------------//

    app.directive('clientCompanies', function() {
        return {
            template: '<li ng-repeat="comp in companies">'+
                '<a ng-href="{{comp.link}}" target="_blank"><img src="/assets/theme/img/patner/{{comp.img}}" alt="{{comp.name}}"></a>'+
            '</li>'
        }
    });
          

//-----------------------//
// Display project links //
//-----------------------//

    app.directive('projLink', function() {
        // TO-DO: Add project image(s)
        return {
            template: '<div class="text-vcenter-area ng-class:hilite;">'+
            '<div class="text-vcenter">'+
                '<h3><a ng-href="{{proj.link}}?on={{dispdetail}}" class="ng-class:linkclss;" ng-click="setdisp(true)">{{proj.title}} <i ng-class="(proj.link==null) ? \'\' : \'fa fa-link\';"></i></a></h3>'+ //Left in "fa-link" ternary if-then-else just for sample
                '<span>{{proj.caption}}</span>'+
            '</div></div>',
            link: function(scope) {
                if (scope.proj.link == null) {
                    scope.linkclss = 'nolink';
                } else if (!scope.proj.link.match(/http/i)) {
                    scope.hilite = 'hilite';
                    scope.linkclss = 'link-work';
                }
            }
        }; 
    });

//})();
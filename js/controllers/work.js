//(function() {

    app.controller('workController', function($routeParams) {
        tab = 1;
        
        this.selectTab = function(setTab) {
            tab = setTab;
        }
        this.isSelected = function(checkTab) {
            return tab === checkTab;
        }
        
        this.indx = $routeParams.indx;

        this.projects = [
            {title: "Teleflora", images: [{img: 'teleflora1.jpg', capt: 'Home Page'}, {img: 'teleflora2.jpg', capt: 'Collection Page'}], 
             descript: {
                 Project: '<p>As one of the top online floral retailers in the country, and as the only one that works exclusively with a network of thousands of local brick-and-motor florists, it\'s essential that Teleflora\'s online pressence is top notch. To that end, a long and complex upgrade project was undertaken that called for a comlplete site redesign, a thorough over-haul of backend systems, and the right technical team to manage, update and maintain the whole thing.</p><p>With the greatest amount of web dev experience on the team, I was given charge of some of the more indespensible tasks of the day-to-day operations, as well as documentation of the new system, and training of various stakeholders.</p>',
                 Details: '<h5>task: <b>Home Page</b></h5><p>The most heavily visited page on the site, the home page is comprised of what\'s referred to as content "cartridges".<ul><li> Manage and maintain all content using Oracle BCC</li><li> Hand-code custom responsive landing pages using Oracle Endeca</li><li> QA all changes for cross-browser compatability, responsiveness, and jQuery based functionality</li></ul></p><h5>task: <b>Collections</b></h5><p>The most "complex" pages on the site, "Collections" are managed through editing of a multitude of different content types.<ul><li> Manage and maintain all content using Oracle BCC</li><li> QA all changes for cross-browser compatability, responsiveness, and jQuery based functionality</li></ul></p><h5>task: <b>Documentation</b></h5><p>Oracel ATG is a complex enterprise level e-commerce system and it was new to the entire team. Having worked on previous integration projects in the past, I was given charge of documenting as much of the custom ATG implementation as possible for use in future trainings.</p>',
                 Tech: '<p>Technologies, systems, platforms, languages, frameworks, and/or libraries used for this project...<ul><li> Oracle BCC</li><li> Oracle Endeca</li><li> HTML5</li><li> CSS3</li><li> jQuery</li></ul></p>'}, 
             cta: [{text: 'Visit Site', link: 'http://www.teleflora.com'}]},
            {title: "Technical Writing", images: [{img: 'spec.png'}], 
             descript: {
                 Project: '<p>When a large, complex, multi-million dollar web site is revamped and relaunched under a strict deadline, inevitably there will be features and functionalities that will not make it into the final product, or will make it but not in their fully flushed out form. As is usually the case, once the new site is in full production and working as expected, those missing or not quite "perfect" features are revisited and work begins on their development.</p><p>I was on the team resposnsible for writing the specs for the development team for all new features, or improving existing ones.</p>',
                 Details: '<p>This particular spec was for the Quick Shop bar on the site. The existing Quick Bar feature was on the top nav bar which disappeared when the user scrolled the page. The new Quick Shop was going to live in a sticky footer bar.</p><p><strong>New Quick Shop Requirements</strong><ul><li> Stick Footer; Provide HTML</li><li> Fully Responsive; Provide CSS</li><li> Provide jQuery code where needed</li><li> Identify where code cleanup is needed</li><li> Fields & copy configurable through Oracel BCC</li></ul></p>'}, 
             cta: [{text: 'Visit Spec', link: '/assets/TFD-1206-Control-of-QuickShop-UI.docx'}, {text: 'Visit Documentation', link: '/assets/Endeca-documentation.docx'}]},
            {title: "Email Generator", images: [{img: 'email1.jpg', capt: 'Responsive Email'}, {img: 'email2.png', capt: 'Preview of XSL in Browser'}, {img: 'email3.png', capt: 'XSL Template'}, {img: 'email4.png', capt: 'Excel Data Sheet'}, {img: 'email5.png', capt: 'XML'}, {img: 'email6.png', capt: 'VBA for Excel'}], 
             descript: {
                 Project: '<p>Email marketing is as big today as it\'s ever been. Any large successful e-commerce business has a robust email marketing plan to not only market and sell their products, but to create a recurring revenue stream. Putting in place such a program requires the right technology, the right resources, and the right partners. Without all of this, any email marketing campaign will slowly die off and fail.</p><p>Teleflora\'s email marketing program was a large and complicated one. It required a lot of time and resources which created an ongoing challenge, especially during peak holidays like Valentine\'s Day, Mother\'s Day and Christmas. The amount of emails that were being sent out was becoming untenable and a better, scalable and sustainable process needed to be implemented.</p><p>The Email Auto-generator was the solution.</p>',
                 Details: '<h5>task: <b>Responsive HTML/CSS Email</b></h5><p>Coded the HTML/CSS for a responsive email.</p><ul><li> A dozen variations of the email template</li><li> Tested on all major email clients and online email providers (desktop, tablet, &amp; mobile)</li><li> Extensive use of Litmus and ReturnPath for QA</li></ul><p></p><h5>task: <b>Excel Link Sheet</b></h5><p>The business needed a simple way to provide all the copy, links, promotion codes, etc. to the developer. An Excel Macro was coded to make it as intuitive as possible for the business making it as easy as possible to provide the required content for the appropriate email template.</p><h5>task: <b>Pre-populate Data</b></h5><p>To improve efficiency and reduce errors, data was pulled directly from the website as opposed to having the business type everything into the link sheet. This feature met the requirement and had the added benefit of ensuring that the email content was always up-to-date.</p><h5>task: <b>Preview &amp; Customization Feature</b></h5><p>The business needed a way to preview the email before it was uploaded to the email testing service. The feature no only provided this preview feature, but with a few clicks here and there, allowed the business to completely change any part of the email to see if it made more sense. If the preview looked good, it was then sent to the developer to QA.</p>',
                 Tech: '<p>Technologies used for this project...</p><ul><li> HTML/CSS</li><li> XML/XSL</li><li> JavaScript/jQuery</li><li> PHP</li><li> Excel VBA</li><li> Windows Shell Script</li><li>FTP</li></ul><p></p>'}, 
             cta: [{text: 'Visit Sample Email', link: 'http://links.mkt2985.com/servlet/MailView?ms=MTU1OTk4MjMS1&r=MzA3OTY0NDc3MTI3S0&j=ODgwNTQyNDkyS0&mt=1&rt=0'}]},
        ]

    });

//})();